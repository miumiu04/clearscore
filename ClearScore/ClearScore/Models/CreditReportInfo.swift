//
//  CreditReportInfo.swift
//  ClearScore
//
//  Created by Mau Geray on 2018/01/30.
//  Copyright © 2018 Mau Geray. All rights reserved.
//

import Foundation

/// Represents the creditReportInfo data.
struct CreditReportInfo {
    var score: Int
    var maxScoreValue: Int
}
