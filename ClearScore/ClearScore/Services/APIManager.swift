//
//  CreditReportAPI.swift
//  ClearScore
//
//  Created by Mau Geray on 2018/01/30.
//  Copyright © 2018 Mau Geray. All rights reserved.
//

import Alamofire

enum FetchCreditReportInfoAPIResponseStatus {
    case success(creditReportInfo: CreditReportInfo)
    case error()
}

/// Handles API calls.
class APIManager {
    /// API Timeout.
    fileprivate static let API_TIMEOUT = 3.0
    
    /// Alamofire request.
    fileprivate static var request: Alamofire.Request?
    /// Alamofire manager.
    fileprivate static let manager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForResource = API_TIMEOUT
        configuration.timeoutIntervalForRequest = API_TIMEOUT
        return SessionManager(configuration: configuration)
    }()
    
    
    /// Fetch credit report info.
    static func fetchCreditReportInfo(completion: @escaping (_ responseStatus: FetchCreditReportInfoAPIResponseStatus) -> Void) {
        let creditReportURL = "https://s3.amazonaws.com/cdn.clearscore.com/native/interview_test/creditReportInfo.json"
        self.request = manager.request(creditReportURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON {
            response in
            // Failed API Connection.
            guard response.result.isSuccess else {
                completion(FetchCreditReportInfoAPIResponseStatus.error())
                return
            }
            
            // Unexpected JSON response.
            guard let jsonResult = response.result.value as? [String : AnyObject]
            else {
                completion(FetchCreditReportInfoAPIResponseStatus.error())
                return
            }
            
            // We need score and maxScoreValue so we need to check if these data are returned from the API.
            guard
                let creditReportInfo = jsonResult["creditReportInfo"] as? [String : AnyObject],
                let score = creditReportInfo["score"] as? Int,
                let maxScoreValue = creditReportInfo["maxScoreValue"] as? Int
            else {
                completion(FetchCreditReportInfoAPIResponseStatus.error())
                return
            }
            
            completion(FetchCreditReportInfoAPIResponseStatus.success(creditReportInfo: CreditReportInfo(score: score, maxScoreValue: maxScoreValue)))
        }
    }
}
