//
//  AppDelegate.swift
//  ClearScore
//
//  Created by Mau Geray on 2018/01/30.
//  Copyright © 2018 Mau Geray. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        return true
    }
}
