//
//  ViewController.swift
//  ClearScore
//
//  Created by Mau Geray on 2018/01/30.
//  Copyright © 2018 Mau Geray. All rights reserved.
//

import UIKit

/// Controls the Dashboard view.
class DashboardViewController: UIViewController {
    @IBOutlet weak var creditScoreProgressBar: CircularProgressBar!
    @IBOutlet weak var creditScoreInfoLabel: UILabel!
    @IBOutlet weak var errorView: UIView!
    @IBAction func retryButtonTapped(_ sender: Any) {
        errorView.isHidden = true
        viewModel.fetchCreditReportInfoFromAPI()
    }
    
    lazy var viewModel: DashboardViewModel = {
        return DashboardViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViewModel()
    }

    func initViewModel() {
        viewModel.errorFetchingCreditReportInfoClosure = {[weak self]() in
            DispatchQueue.main.async {
                self?.errorView.isHidden = false
            }
        }
        
        viewModel.successFetchingCreditReportInfoClosure = {[weak self]() in
            DispatchQueue.main.async {
                self?.updateCreditScoreData()
            }
        }
        viewModel.fetchCreditReportInfoFromAPI()
    }

    /// Updates the credit score data i.e. creditScoreProgressBar and creditScoreInfoLabel
    func updateCreditScoreData() {
        // Update the creditScoreProgressBar
        creditScoreProgressBar.animateProgressCircle(toValue: CGFloat(viewModel.creditScorePercentage), duration: 1.0)
        
        // Update the creditScoreInfoLabel
        let creditScoreAttrStr = NSMutableAttributedString()
        creditScoreAttrStr.append(getCreditScoreInfoAttrText(viewModel.creditScoreHeader))
        creditScoreAttrStr.append(NSAttributedString(string: "\n"))
        creditScoreAttrStr.append(getCreditScoreText())
        creditScoreAttrStr.append(NSAttributedString(string: "\n"))
        creditScoreAttrStr.append(getCreditScoreInfoAttrText(viewModel.creditScoreFooter))
        creditScoreInfoLabel.attributedText = creditScoreAttrStr
    }
    
    /// Returns the attributed text using the same properties defined in creditScoreInfoLabel.
    /// - Parameter text: The text to be used.
    private func getCreditScoreInfoAttrText(_ text: String) -> NSAttributedString {
        let creditScoreHeaderAttrStr = NSAttributedString(string: text,
                                               attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: creditScoreInfoLabel.font.pointSize)])
        return creditScoreHeaderAttrStr
    }
    
    /// Returns the attributed text with font 5x larger than the fontsize used in creditScoreInfoLabel.
    /// This also used the thin font weight.
    private func getCreditScoreText() -> NSAttributedString {
        let creditScoreFontSize = creditScoreInfoLabel.font.pointSize * 5
        let creditScoreColor = creditScoreProgressBar.progressCircleColor
        let creditScoreAttrStr = NSAttributedString(string: viewModel.creditScore,
                                                    attributes: [NSAttributedStringKey.foregroundColor: creditScoreColor,
                                                                 NSAttributedStringKey.font: UIFont.systemFont(ofSize: creditScoreFontSize, weight: .thin)])
        return creditScoreAttrStr
    }
}
