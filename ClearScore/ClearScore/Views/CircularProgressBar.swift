//
//  CircularProgressBar.swift
//  ClearScore
//
//  Created by Mau Geray on 2018/01/30.
//  Copyright © 2018 Mau Geray. All rights reserved.
//

import UIKit

/// Custom view that displays a full circle with a progress circle inside.
@IBDesignable class CircularProgressBar: UIView {
    
    /// Color of the full circle. Default color is black.
    @IBInspectable var fullCircleColor: UIColor = UIColor.black {
        didSet {
            if fullCircleLayer != nil {
                fullCircleLayer.strokeColor = fullCircleColor.cgColor
            }
        }
    }
    
    /// Color of the progress circle. Default color is black.
    /// Note that we are actually using a gradient (progressCircleColor and white)
    @IBInspectable var progressCircleColor: UIColor = UIColor.black {
        didSet {
            if progressCircleLayer != nil {
                progressCircleLayer.strokeColor = progressCircleColor.cgColor
                gradientLayer.colors = [progressCircleColor.cgColor, UIColor.white.cgColor]
            }
        }
    }
    
    /// Width of the stroke of the circles. Default value is 5.
    @IBInspectable var circleStrokeWidth: CGFloat = 5 {
        didSet {
            if fullCircleLayer != nil && progressCircleLayer != nil {
                fullCircleLayer.lineWidth = circleStrokeWidth
                progressCircleLayer.lineWidth = circleStrokeWidth
            }
        }
    }
    
    var fullCircleLayer:CAShapeLayer!
    var progressCircleLayer: CAShapeLayer!
    var gradientLayer = CAGradientLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    func setup() {
        layoutFullCircle()
        layoutProgressCircle()
    }
    
    func layoutFullCircle() {
        if fullCircleLayer == nil {
            fullCircleLayer = CAShapeLayer()
            let radius: CGFloat = min(bounds.size.width/2, bounds.size.height/2)
            let fullCirclePath = UIBezierPath(arcCenter: CGPoint(x: radius, y: radius),
                                              radius: radius,
                                              startAngle: 0,
                                              endAngle: CGFloat(Double.pi * 2),
                                              clockwise: true)
            fullCircleLayer.path = fullCirclePath.cgPath
            fullCircleLayer.fillColor = UIColor.clear.cgColor
            fullCircleLayer.strokeColor = fullCircleColor.cgColor
            fullCircleLayer.lineWidth = circleStrokeWidth
            fullCircleLayer.strokeEnd = 1.0
            
            layer.addSublayer(fullCircleLayer)
        }
    }
    
    func layoutProgressCircle() {
        if progressCircleLayer == nil {
            progressCircleLayer = CAShapeLayer()
            let radius: CGFloat = min(bounds.size.width/2, bounds.size.height/2)
            let progressCirclePath = UIBezierPath(arcCenter: CGPoint(x: radius, y: radius),
                                                  radius: radius * 0.9, // 90% of the size of the full circle
                                                  startAngle: -CGFloat(Double.pi/2), // top of the circle
                                                  endAngle: CGFloat(Double.pi + Double.pi/2), // end of the circle
                                                  clockwise: true)
            progressCircleLayer.path = progressCirclePath.cgPath
            progressCircleLayer.fillColor = UIColor.clear.cgColor
            progressCircleLayer.strokeColor = progressCircleColor.cgColor
            progressCircleLayer.lineWidth = circleStrokeWidth
            progressCircleLayer.strokeEnd = 1.0
            
            gradientLayer.frame = CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: self.bounds.size)
            gradientLayer.colors = [progressCircleColor.cgColor, UIColor.white.cgColor]
            gradientLayer.startPoint = CGPoint(x: 0.7, y: 0.7)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
            gradientLayer.mask = progressCircleLayer
            
            layer.addSublayer(gradientLayer)
        }
    }
    
    /// Displays the progress circle with animation.
    /// - Parameter toValue: The value until where the progress circle will be drawn. Value must be between 0 and 1.
    /// - Parameter duration: The duration of the animation, in seconds.
    func animateProgressCircle(toValue: CGFloat, duration: TimeInterval) {
        if progressCircleLayer != nil {
            progressCircleLayer.strokeEnd = 0
            
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.duration = duration
            animation.fromValue = 0
            animation.toValue = toValue
            animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            
            progressCircleLayer.strokeEnd = toValue
            progressCircleLayer.add(animation, forKey: "animateProgressCircle")
        }
    }
    
}
