//
//  DashboardViewModel.swift
//  ClearScore
//
//  Created by Mau Geray on 2018/01/30.
//  Copyright © 2018 Mau Geray. All rights reserved.
//

import Foundation

/// Handles the logic related to the Dashboard view.
class DashboardViewModel {
    var errorFetchingCreditReportInfoClosure: (()->())?
    var successFetchingCreditReportInfoClosure: (()->())?
    
    var creditReportInfo: CreditReportInfo = CreditReportInfo(score: 0, maxScoreValue: 0) {
        didSet {
            self.successFetchingCreditReportInfoClosure?()
        }
    }
    
    func fetchCreditReportInfoFromAPI() {
        APIManager.fetchCreditReportInfo { (responseStatus) in
            switch responseStatus {
            case .error():
                self.errorFetchingCreditReportInfoClosure?()
            case .success(let creditReportInfo):
                self.creditReportInfo = creditReportInfo
            }
        }
    }
    
    var creditScoreHeader: String {
        return "Your credit score is"
    }
    
    var creditScoreFooter: String {
        return String(format: "out of %d", creditReportInfo.maxScoreValue)
    }
    
    var creditScore: String {
        return String(creditReportInfo.score)
    }
    
    var creditScorePercentage: Double {
        if creditReportInfo.maxScoreValue == 0 {
            return 0
        }
        return Double(creditReportInfo.score) / Double(creditReportInfo.maxScoreValue)
    }
}
