//
//  DashboardViewModelTests.swift
//  ClearScoreTests
//
//  Created by Mau Geray on 2018/01/30.
//  Copyright © 2018 Mau Geray. All rights reserved.
//

import XCTest

class DashboardViewModelTests: XCTestCase {
    
    var viewModel: DashboardViewModel!
    
    override func setUp() {
        super.setUp()
        viewModel = DashboardViewModel()
    }
    
    func testInitialValues() {
        XCTAssertEqual(viewModel.creditScoreHeader, "Your credit score is")
        XCTAssertEqual(viewModel.creditScoreFooter, "out of 0")
        XCTAssertEqual(viewModel.creditScore, "0")
        XCTAssertEqual(viewModel.creditScorePercentage, 0)
    }
    
    func testValuesFromAPI() {
        viewModel.errorFetchingCreditReportInfoClosure = {[weak self]() in
            DispatchQueue.main.async {
                XCTFail("Failed getting data")
            }
        }
        
        viewModel.successFetchingCreditReportInfoClosure = {[weak self]() in
            DispatchQueue.main.async {
                XCTAssertEqual(self?.viewModel.creditScoreHeader, "Your credit score is")
                XCTAssertEqual(self?.viewModel.creditScoreFooter, "out of 700")
                XCTAssertEqual(self?.viewModel.creditScore, "514")
                XCTAssertEqual(self?.viewModel.creditScorePercentage, 514.0/700.0)
            }
        }
        viewModel.fetchCreditReportInfoFromAPI()
    }
    
    func testValues() {
        viewModel.creditReportInfo = CreditReportInfo(score: 100, maxScoreValue: 500)
        XCTAssertEqual(viewModel.creditScoreHeader, "Your credit score is")
        XCTAssertEqual(viewModel.creditScoreFooter, "out of 500")
        XCTAssertEqual(viewModel.creditScore, "100")
        XCTAssertEqual(viewModel.creditScorePercentage, 100.0/500.0)
        
    }
}
