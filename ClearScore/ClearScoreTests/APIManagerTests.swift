//
//  APIManagerTests.swift
//  ClearScoreTests
//
//  Created by Mau Geray on 2018/01/30.
//  Copyright © 2018 Mau Geray. All rights reserved.
//

import XCTest

class APIManagerTests: XCTestCase {
    
    func testFetchCreditReportInfo() {
        APIManager.fetchCreditReportInfo { (responseStatus) in
            switch responseStatus {
            case .error():
                XCTFail("Failed getting data")
            case .success(let creditReportInfo):
                XCTAssertNotNil(creditReportInfo)
                XCTAssertTrue(creditReportInfo.score >= 0)
                XCTAssertTrue(creditReportInfo.maxScoreValue >= 0)
            }
        }
    }
}
